const {
  getAllUsers,
  getUserByID,
  createNewUser,
  updateUserByID,
  deleteUser,
} = require("./user/user");

// console.log(getAllUsers());

// console.log(getUserByID(1));

// console.log(
//   createNewUser({
//     fName: "Test",
//     lName: "User",
//     username: "testUser",
//   })
// );

// console.log(updateUserByID(5, { fName: "example", username: "example" }));

// console.log(deleteUser(4));
